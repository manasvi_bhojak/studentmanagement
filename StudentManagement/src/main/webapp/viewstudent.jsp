<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div align="center">
<h1>View Student Details</h1>
<table border="1">
<tr>
	<th>StudentId</th>
	 <th>First Name</th>
	 <th>Middle Name</th>
	 <th>Last Name</th>
	 <th>Student Course</th>
	 <th>Student Gender</th>
	 <th>PhoneNo</th>
	<th>Student Address</th>
	 <th>Email</th>
	 <th>Language</th>
	 <th colspan="2">Action</th>
</tr>

<c:forEach items="${studentList}" var="student">
<tr>
 	<td>${student.studentId }</td>
	 <td>${student.firstName}</td>
	<td>${student.middleName}</td>
	 <td>${student.lastName}</td>
	<td>${student.studentCourse}</td>
	<td>${student.studentGender}</td>
	<td>${student.phoneNo}</td>
	<td>${student.studentAddress}</td>
	<td>${student.email}</td>
	 <td>${student.language}</td>
	 <td><a href="StudentController?action=edit&studentId=${student.studentId }">edit</a></td>
	 <td><a href="StudentController?action=delete&studentId=${student.studentId }">delete</a></td>
	</tr>
</c:forEach>

</table>
</div>
</body>
</html>