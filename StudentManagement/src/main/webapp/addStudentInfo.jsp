<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<!DOCTYPE html>
<html>
<head>
<style type="text/css">
input[type=text], select, textarea {
 	width: 35%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  float: center;
}

input[type=submit]:hover {
  background-color: #45a049;
}


</style>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div align="center">
<form action="StudentController" method="post">
<h1>Add Student Information</h1>
<input type="hidden" name="studentId" value="${Student.studentId}">
<br><br>

Enter the First Name : 
<input type="text" name="firstName"  value="${Student.firstName}" required="required">
<br><br>
Enter the Middle Name :
<input type="text" name="middleName" value="${Student.middleName}" required="required">
<br><br>
Enter the Last Name :
<input type="text" name="lastName" value="${Student.lastName}" required="required">
<br><br>
Select Course : <select name="course">
  <option value="BCS" <c:if test="${Student.studentCourse == 'BCS'}">  selected="selected"  </c:if> >BCS</option>
  <option value="MCS" <c:if test="${Student.studentCourse == 'MCS'}">selected="selected"</c:if>>MCS</option>
  <option value="BCA" <c:if test="${Student.studentCourse == 'BCA'}">selected="selected"</c:if>>BCA</option>
  <option value="MCA" <c:if test="${Student.studentCourse == 'MCA'}">selected="selected"</c:if>>MCA</option>
</select>

<br><br>
Select Gender : <input type="radio"  name="gender" value="male" <c:if test="${Student.studentGender == 'male'}">checked="checked"</c:if> > Male  
<input type="radio"  name="gender" value="female" <c:if test="${Student.studentGender == 'female'}">checked="checked"</c:if>> Female

<br><br>
Enter the Phone Number :
<input type="text" name="phoneNo" value="${Student.phoneNo}" required="required">

<br><br>
Enter the Address :
<textarea  name="address" required="required">${Student.studentAddress}</textarea>

<br><br>
Enter the Email :
<input type="text" name="email" value="${Student.email}" required="required">


<br><br>
Select Language
<input type="checkbox"  name="language" value="english" <c:if test="${fn:contains(Student.language, 'english')}">checked="checked"</c:if> >English
<input type="checkbox"  name="language" value="hindi" <c:if test="${fn:contains(Student.language, 'hindi')}">checked="checked"</c:if> >Hindi
<input type="checkbox"  name="language" value="marathi" <c:if test="${fn:contains(Student.language, 'marathi')}">checked="checked"</c:if> >Marathi
<input type="checkbox"  name="language" value="gujrati" <c:if test="${fn:contains(Student.language, 'gujrati')}">checked="checked"</c:if> >Gujrathi



<br><br>
<input type="submit" value="${BtnName}" >
<br><br>

<a href="index.jsp">Go to Menu</a>
</form>
</div>

</body>
</html>