package com.web.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.web.model.Student;
import com.web.repository.StudentRepository;

/**
 * Servlet implementation class StudentController
 */
@WebServlet("/StudentController")
public class StudentController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String action = request.getParameter("action");
		if(action.equals("add")) {
			
			
			request.setAttribute("BtnName","Add");
			RequestDispatcher rd = request.getRequestDispatcher("addStudentInfo.jsp");
			rd.forward(request, response);
		}else if(action.equals("view")) {
			
			StudentRepository studRespo = new StudentRepository();
			List<Student> studentList = studRespo.viewStudent();
			request.setAttribute("studentList", studentList);
			RequestDispatcher rd = request.getRequestDispatcher("viewstudent.jsp");
			rd.forward(request, response);
		}else if(action.equals("delete")) {
			
			
			int studentId = Integer.parseInt(request.getParameter("studentId"));
			System.out.println(studentId);
			StudentRepository studRespo = new StudentRepository();
			studRespo.deleteStudent(studentId);
			
			List<Student> studentList = studRespo.viewStudent();
			request.setAttribute("studentList", studentList);
			RequestDispatcher rd = request.getRequestDispatcher("viewstudent.jsp");
			rd.forward(request, response);
		}else if(action.equals("edit")) {
			
			request.setAttribute("BtnName","Edit");
			int studentId = Integer.parseInt(request.getParameter("studentId"));
			StudentRepository studRespo = new StudentRepository();
			Student student = studRespo.getStudentById(studentId);
			request.setAttribute("Student", student);
			RequestDispatcher rd = request.getRequestDispatcher("addStudentInfo.jsp");
			rd.forward(request, response);
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 String studentId = request.getParameter("studentId");
		 String firstName = request.getParameter("firstName");
		 String middleName = request.getParameter("middleName");
		 String lastName  = request.getParameter("lastName");
		 String studentCourse = request.getParameter("course");
		 String studentGender = request.getParameter("gender");
		 String phoneNo = request.getParameter("phoneNo");
		 String studentAddress = request.getParameter("address");
		 String email  = request.getParameter("email");
		 String language = "";
		 String[] lang = request.getParameterValues("language");
		 for(int i=0;i<lang.length;i++){
			    language+=lang[i]+",";
		 }
		 Student stud = new Student();
		 stud.setFirstName(firstName);
		 stud.setMiddleName(middleName);
		 stud.setLastName(lastName);
		 stud.setStudentCourse(studentCourse);
		 stud.setStudentGender(studentGender);
		 stud.setPhoneNo(phoneNo);
		 stud.setStudentAddress(studentAddress);
		 stud.setEmail(email);
		 stud.setLanguage(language);
	
		 if(studentId != "" || !studentId.isEmpty())
		 {
			 stud.setStudentId(Integer.parseInt(studentId));
			 StudentRepository studRespo = new StudentRepository();
			 studRespo.updateStudent(stud);
			 
			 List<Student> studentList = studRespo.viewStudent();
			 request.setAttribute("studentList", studentList);
			 RequestDispatcher rd = request.getRequestDispatcher("viewstudent.jsp");
			 rd.forward(request, response);
		 }
		 else {
			 System.out.println(stud);
			 StudentRepository studRespo = new StudentRepository();
			 studRespo.addStudentInfo(stud);
			 response.sendRedirect("addStudentInfo.jsp");
		 }
	
	}

	
}
