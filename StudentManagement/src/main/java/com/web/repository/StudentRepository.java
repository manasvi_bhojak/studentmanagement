package com.web.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.web.model.Student;

public class StudentRepository {
	
	public void addStudentInfo(Student stud) {
		
		Connection con = DBConnection.getDBConnection();
		PreparedStatement pstmt = null;
		try {
			
			pstmt = con.prepareStatement("insert into studentinfo(first_name,middle_name,last_name,student_course,student_gender,phone_no,student_address,email,language) values(?,?,?,?,?,?,?,?,?)");
			pstmt.setString(1,stud.getFirstName());
			pstmt.setString(2,stud.getMiddleName());
			pstmt.setString(3,stud.getLastName());
			pstmt.setString(4,stud.getStudentCourse());
			pstmt.setString(5,stud.getStudentGender());
			pstmt.setString(6,stud.getPhoneNo());
			pstmt.setString(7,stud.getStudentAddress());
			pstmt.setString(8,stud.getEmail());
			pstmt.setString(9,stud.getLanguage());
			int status = pstmt.executeUpdate();
			System.out.println(status +" record inserted");
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			try {
				pstmt.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	
	public List<Student> viewStudent() {
		
		List<Student> studentList = null; 
		Connection con = DBConnection.getDBConnection();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery("select * from studentinfo");
			studentList = new ArrayList<Student>();
			while(rs.next()) {
				Student stud = new Student();
				stud.setStudentId(rs.getInt(1));
				stud.setFirstName(rs.getString(2));
				stud.setMiddleName(rs.getString(3));
				stud.setLastName(rs.getString(4));
				stud.setStudentCourse(rs.getString(5));
				stud.setStudentGender(rs.getString(6));
				stud.setPhoneNo(rs.getString(7));
				stud.setStudentAddress(rs.getString(8));
				stud.setEmail(rs.getString(9));
				stud.setLanguage(rs.getString(10));
				studentList.add(stud);
			}
		
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				rs.close();
				stmt.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return studentList;
		
	}
	
	public void deleteStudent(int studentId) {
		
		Connection con = DBConnection.getDBConnection();
		PreparedStatement pstmt =null;
		try {
			
			pstmt = con.prepareStatement("delete from studentinfo where Student_Id = ?");
			pstmt.setInt(1,studentId);
			int status = pstmt.executeUpdate();
			System.out.println(status +" record delete");
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				pstmt.close();
				con.close();

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public Student getStudentById(int studentId) {
		Student stud = null;
		Connection con = DBConnection.getDBConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			pstmt = con.prepareStatement("select * from studentinfo where student_id = ?");
			pstmt.setInt(1, studentId);
			rs = pstmt.executeQuery();
			stud = new Student();
			if(rs.next()) {
				
				stud.setStudentId(rs.getInt(1));
				stud.setFirstName(rs.getString(2));
				stud.setMiddleName(rs.getString(3));
				stud.setLastName(rs.getString(4));
				stud.setStudentCourse(rs.getString(5));
				stud.setStudentGender(rs.getString(6));
				stud.setPhoneNo(rs.getString(7));
				stud.setStudentAddress(rs.getString(8));
				stud.setEmail(rs.getString(9));
				stud.setLanguage(rs.getString(10));
				
			}
			
		}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally {
				try {
					pstmt.close();
					con.close();

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
		
		return stud;
		
		
	}
	
	
	
	public void updateStudent(Student stud) {
		Connection con = DBConnection.getDBConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("update studentinfo set first_name = ?,middle_name = ?,last_name = ?, student_course = ? ,student_gender = ?,phone_no = ?,student_address = ?,email = ?,language =? where Student_id = ?");
			pstmt.setString(1,stud.getFirstName());
			pstmt.setString(2,stud.getMiddleName());
			pstmt.setString(3,stud.getLastName());
			pstmt.setString(4,stud.getStudentCourse());
			pstmt.setString(5,stud.getStudentGender());
			pstmt.setString(6,stud.getPhoneNo());
			pstmt.setString(7,stud.getStudentAddress());
			pstmt.setString(8,stud.getEmail());
			pstmt.setString(9,stud.getLanguage());
			pstmt.setInt(10,stud.getStudentId());
			int status = pstmt.executeUpdate();
			System.out.println(status +" record updated");
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


